﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;


namespace AgenteDePrestamos
{
    public partial class Main : Form
    {
        public SqlConnection con = null;

        public string DataSourceText;

        public string DataBaseText;

        private string UserText;

        private string PasswordText;

        public double Porcentaje;

        public string Prestamo;

        public string Nominas;

        public string Observacion;

        public int ProcesaPrestamo;

        public List<string> DiasDePago = new List<string>();

        public bool ValidacionSolPrestamoActivo;
        public Main()
        {
            this.InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            if (this.ValidarUsuario())
            {

                string location = Assembly.GetExecutingAssembly().Location;
                string directoryName = Path.GetDirectoryName(location);

                if (File.Exists(directoryName + "/Setup.xml"))
                {

                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(directoryName + "/Setup.xml");
                    XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("Configuracion");
                    XmlNodeList elementsByTagName2 = ((XmlElement)elementsByTagName[0]).GetElementsByTagName("Servidor");
                    XmlNodeList elementsByTagName3 = ((XmlElement)elementsByTagName[0]).GetElementsByTagName("Extras");

                    foreach (XmlElement xmlElement in elementsByTagName2)
                    {
                        int i = 0;

                        XmlNodeList elementsByTagName4 = xmlElement.GetElementsByTagName("DataSource");
                        XmlNodeList elementsByTagName5 = xmlElement.GetElementsByTagName("DataBase");
                        this.DataSourceText = elementsByTagName4[i].InnerText;
                        this.DataBaseText = elementsByTagName5[i].InnerText;

                        XmlNodeList UserDB = xmlElement.GetElementsByTagName("User");
                        XmlNodeList PassDB = xmlElement.GetElementsByTagName("Pwd");

                        if (UserDB.Count == elementsByTagName5.Count)
                            UserText = UserDB[i].InnerText;
                        else
                            UserText = "SA";

                        if (PassDB.Count == elementsByTagName5.Count)
                            PasswordText = PassDB[i].InnerText;
                        else
                            PasswordText = String.Empty;

                        i++;
                    }

                    foreach (XmlElement xmlElement in elementsByTagName3)
                    {
                        int i = 0;
                        XmlNodeList elementsByTagName6 = xmlElement.GetElementsByTagName("Porcentaje");
                        this.Porcentaje = double.Parse(elementsByTagName6[i].InnerText) / 100.0;
                        XmlNodeList elementsByTagName7 = xmlElement.GetElementsByTagName("CodPrestamo");
                        this.Prestamo = elementsByTagName7[i].InnerText;
                        XmlNodeList elementsByTagName8 = xmlElement.GetElementsByTagName("Nominas");
                        this.Nominas = elementsByTagName8[i].InnerText;
                        this.Nominas = this.Nominas.Trim();
                        XmlNodeList elementsByTagName9 = xmlElement.GetElementsByTagName("ProcesarPrestamo");
                        this.ProcesaPrestamo = int.Parse(elementsByTagName9[i].InnerText);
                        XmlNodeList elementsByTagName10 = xmlElement.GetElementsByTagName("Observacion");
                        this.Observacion = elementsByTagName10[i].InnerText;
                        i++;
                    }

                    String fecha_actual = DateTime.Today.ToString("yyyyMMdd");

                    this.con = new SqlConnection();
                    this.con.ConnectionString = string.Concat(new string[]
					{
						"Data Source=",
						this.DataSourceText,
						";Initial Catalog=",
						this.DataBaseText,
						";User ID=" + UserText + ";Password=" + PasswordText
					});

                    try
                    {

                        this.con.Open();

                        string codCorrida = this.Consecutivo(false);

                        string text = "";

                        if (this.ValNominas())
                        {
                            if (this.Nominas.CompareTo("") == 0)
                            {
                                text = "";
                            }
                            else
                            {
                                string[] array = this.Nominas.Split(new char[]
								{
									','
								});
                                string[] array2 = array;
                                for (int j = 0; j < array2.Length; j++)
                                {
                                    string text2 = array2[j];
                                    string text3 = text2.Trim();
                                    if (text3.CompareTo("") != 0)
                                    {
                                        if (text.CompareTo("") == 0)
                                        {
                                            text = text + " WHERE CU_CODIGO = '" + text3 + "'";
                                        }
                                        else
                                        {
                                            text = text + " OR CU_CODIGO = '" + text3 + "'";
                                        }
                                    }
                                }
                            }
                        }

                        DataSet dataSet = new DataSet();
                        string selectCommandText = "SELECT * FROM MA_NOMINA " + text;
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommandText, this.con);
                        sqlDataAdapter.Fill(dataSet, "Data");

                        if (dataSet.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                            {
                                DataRow dataRow = dataSet.Tables["Data"].Rows[i];
                                string text2 = dataRow["cu_codigo"].ToString();
                                DataSet dataSet2 = new DataSet();
                                string selectCommandText2 = "SELECT * FROM MA_NOMINA_DIASDEPAGO where cu_codnomina='" + text2 + "' order by nu_pos";
                                SqlDataAdapter sqlDataAdapter2 = new SqlDataAdapter(selectCommandText2, this.con);
                                sqlDataAdapter2.Fill(dataSet2, "Data");
                                if (dataSet2.Tables[0].Rows.Count > 0)
                                {
                                    for (int k = 0; k < dataSet2.Tables[0].Rows.Count; k++)
                                    {
                                        DataRow dataRow2 = dataSet2.Tables["Data"].Rows[k];
                                        this.DiasDePago.Add(dataRow2["nu_diacorte"].ToString());
                                    }
                                }
                                DataSet dataSet3 = new DataSet();
                                string selectCommandText3 = "SELECT *, DAY(getdate()) as DiaActual, case when month(du_fechaIngreso) = MONTH(getdate()) AND YEAR(DU_FECHAINGRESO) = YEAR(GETDATE()) then DATEDIFF(day,du_fechaIngreso,GETDATE()) else '-999' end as DifFechaIngreso from MA_PERSONAL inner join MA_ESTATUS_EMPLEADO on MA_PERSONAL.cu_codEstatus = MA_ESTATUS_EMPLEADO.cu_codigo where cu_codnomina='" + text2 + "' and MA_ESTATUS_EMPLEADO.cu_codigo = '0000000001' AND convert(varchar, du_fechaingreso, 112) < convert(varchar, getdate(), 112)";
                                SqlDataAdapter sqlDataAdapter3 = new SqlDataAdapter(selectCommandText3, this.con);
                                sqlDataAdapter3.Fill(dataSet3, "Data");
                                if (dataSet3.Tables[0].Rows.Count > 0)
                                {
                                    for (int l = 0; l < dataSet3.Tables[0].Rows.Count; l++)
                                    {
                                        DataRow dataRow3 = dataSet3.Tables["Data"].Rows[l];
                                        string text4 = dataRow3["cu_codigo"].ToString();
                                        double num = (double)(float.Parse(dataRow3["nu_sueldo"].ToString()) / 30f);
                                        int num2 = int.Parse(dataRow3["DifFechaIngreso"].ToString());
                                        int num3 = int.Parse(dataRow3["DiaActual"].ToString());
                                        if (num2 > 0)
                                        {
                                            num2 = num3 - num2;
                                        }
                                        int num4 = this.DiasTrabajados(text4, num2);
                                        double monto = Math.Round(num * (double)num4 * this.Porcentaje, 2);
                                        if (this.ValidarSolicitudesActivas(text4))
                                        {
                                            if (this.InsertSolPrestamo(dataRow3["cu_codigo"].ToString(), monto, fecha_actual, codCorrida))
                                            {
                                                if (this.ProcesaPrestamo != 0)
                                                {
                                                    if (this.ProcesarPrestamo(dataRow3["cu_codigo"].ToString(), monto, fecha_actual, int.Parse(this.CodSolicitudPrestamo(dataRow3["cu_codigo"].ToString())), codCorrida))
                                                    {
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Al menos 1 empleado posee el mismo Préstamo pendiente por aprobar. Si desea Modificarlo, elimine dicho préstamo y vuélvaselo a asignar.");
                                            Environment.Exit(0);
                                        }
                                    }
                                }
                            }
                        }
                        if (this.CrearCorridaPrestamo(codCorrida))
                        {
                            codCorrida = this.Consecutivo(true);
                        }
                    }
                    catch (Exception arg)
                    {
                        MessageBox.Show("Error al abrir la conexión: " + arg);
                    }
                    finally
                    {
                        if (this.con.State != null)
                            if (this.con.State != ConnectionState.Closed)
                                try { this.con.Close(); } catch (Exception) { }
                    }
                }
                else
                {
                    MessageBox.Show("Disculpe, El Archivo de configuración no existe");
                }

                MessageBox.Show("Agente Ejecutado Satisfactoriamente, Para saber el Resultado del mismo Revisar las solicitudes de Prestamos.");
                base.Close();

            }
            else
            {
                MessageBox.Show("Usuario No Validado.");
                base.Close();
            }

        }

        public int DiasTrabajados(string CodEmpleado, int DiferenciaFechaIngreso)
        {
            int DiasTrabajados = 0;
            if (DiferenciaFechaIngreso > 0)
            {
                DiasTrabajados = 30 - DiferenciaFechaIngreso;
            }
            else
            {   
                int DiasDeVacaciones = DiasVacaciones(CodEmpleado);
                if (DiasDeVacaciones > 0)
                {
                    DiasTrabajados = 30 - DiasDeVacaciones;
                }
                else
                {
                    int DiasDeReposo = DiasReposos(CodEmpleado);
                    if (DiasDeReposo > 0)
                    {
                        DiasTrabajados = 30 - DiasDeReposo;
                    }
                    else
                    {
                        DiasTrabajados = 30;
                    }
                }
            }
            return DiasTrabajados;
        }

        public int DiasVacaciones(string CodEmpleado)
        {
            //con = new System.Data.SqlClient.SqlConnection();
            //con.ConnectionString = "Data Source=" + DataSourceText + ";Initial Catalog=" + DataBaseText + ";User ID=sa;Password=";
            int Dias = 0;
            try
            {
                if (this.con.State == ConnectionState.Closed)
                {
                    this.con.Open();
                }
                DataSet DataSetVacaciones = new DataSet();
                //String QueryVacaciones = "select case when MONTH(du_fechavacas) = MONTH(du_fecharegreso) then DATEDIFF(day,du_fechavacas,du_fecharegreso) - 1	else DATEDIFF(day,CAST(CAST(YEAR(GETDATE()) AS VARCHAR)+'-'+CAST(MONTH(GETDATE()) AS VARCHAR)+'-'+CAST(01 AS VARCHAR) as date), du_fecharegreso) - 1  end AS DiasDeVacaciones from TR_VACACIONES_DIASDEDISFRUTE where MONTH(GETDATE()) = MONTH(du_fecharegreso) and YEAR(GETDATE()) = YEAR(du_fecharegreso) AND cu_codpersonal = '"+CodEmpleado+"'";
                String QueryVacaciones = "select du_fechavacas, du_fecharegreso from TR_VACACIONES_DIASDEDISFRUTE where YEAR(du_fecharegreso) = YEAR(GETDATE()) AND MONTH(du_fecharegreso) = MONTH(GETDATE()) AND DAY(du_fecharegreso) < DAY(GETDATE()) AND cu_codpersonal = '" + CodEmpleado + "'";
                SqlDataAdapter ComandoSqlAdapterVacaciones = new SqlDataAdapter(QueryVacaciones, con);
                ComandoSqlAdapterVacaciones.Fill(DataSetVacaciones, "Data");
                if (DataSetVacaciones.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DataSetVacaciones.Tables[0].Rows.Count; i++)
                    {
                        DataRow campoDiasVacaciones = DataSetVacaciones.Tables["Data"].Rows[i];

                        if (DiasDePago.Count == 1)
                        {
                            int Mes, Ano, MesA, AnoA;
                            DateTime Today = DateTime.Now;
                            Mes = Today.Month;
                            Ano = Today.Year;
                            if (Mes == 1)
                            {
                                MesA = 12;
                                AnoA = Ano - 1;
                            }
                            else
                            {
                                MesA = Mes - 1;
                                AnoA = Ano;
                            }
                            int DiasDelMesAnterior = DateTime.DaysInMonth(AnoA, MesA);
                            int DiaDeCorte = Int32.Parse(DiasDePago[0]) + 1;
                            int TempDias = DiaDeCorte - DiasDelMesAnterior;
                            if (TempDias >= 0)
                            {
                                DiaDeCorte = DiaDeCorte - TempDias;
                            }
                            DateTime Desde = DateTime.Parse(AnoA + "-" + MesA + "-" + DiaDeCorte);
                            DateTime Hasta = DateTime.Parse(campoDiasVacaciones["du_fecharegreso"].ToString());
                            TimeSpan DiferenciaDias = (Desde - Hasta);
                            Dias = DiferenciaDias.Days * -1;
                        }
                    }
                }
                //con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
            finally
            {
            }
            return Dias;
        }

        public int DiasReposos(string CodEmpleado)
        {
            //con = new System.Data.SqlClient.SqlConnection();
            //con.ConnectionString = "Data Source=" + DataSourceText + ";Initial Catalog=" + DataBaseText + ";User ID=sa;Password=";
            int Dias = 0;
            try
            {
                if (this.con.State == ConnectionState.Closed)
                {
                    this.con.Open();
                }

                DataSet DataSetReposos = new DataSet();
                String QueryReposos = "select du_FechaInicio, du_FechaFin from MA_REPOSOS where cu_CodigoPersonal ='"+CodEmpleado+"' and  YEAR(du_FechaFin) = YEAR(GETDATE()) AND MONTH(du_FechaFin) = MONTH(GETDATE()) AND DAY(du_FechaFin) < DAY(GETDATE())";
                SqlDataAdapter ComandoSqlAdapterReposos = new SqlDataAdapter(QueryReposos, con);
                ComandoSqlAdapterReposos.Fill(DataSetReposos, "Data");
                if (DataSetReposos.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DataSetReposos.Tables[0].Rows.Count; i++)
                    {
                        DataRow campoDiasVacaciones = DataSetReposos.Tables["Data"].Rows[i];

                        if (DiasDePago.Count == 1)
                        {
                            int Mes, Ano, MesA, AnoA;
                            DateTime Today = DateTime.Now;
                            Mes = Today.Month;
                            Ano = Today.Year;
                            if (Mes == 1)
                            {
                                MesA = 12;
                                AnoA = Ano - 1;
                            }
                            else
                            {
                                MesA = Mes - 1;
                                AnoA = Ano;
                            }
                            int DiasDelMesAnterior = DateTime.DaysInMonth(AnoA, MesA);
                            int DiaDeCorte = Int32.Parse(DiasDePago[0]) + 1;
                            int TempDias = DiaDeCorte - DiasDelMesAnterior;
                            if (TempDias >= 0)
                            {
                                DiaDeCorte = DiaDeCorte - TempDias;
                            }
                            DateTime Desde = DateTime.Parse(AnoA + "-" + MesA + "-" + DiaDeCorte);
                            DateTime Hasta = DateTime.Parse(campoDiasVacaciones["du_fechaFin"].ToString());
                            TimeSpan DiferenciaDias = (Desde - Hasta);
                            Dias = DiferenciaDias.Days * -1;
                        }
                    }
                }
                //con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
            finally
            {

            }

            return Dias;
        }

        
        public Boolean InsertSolPrestamo(string CodEmp, Double Monto, string fecha_actual)
        {
            bool Bandera = false;
            int Estatus = 0, Cancelado = 0;
            if (ProcesaPrestamo != 0)
            {
                Estatus = 1;
                Cancelado = 1;
            }

            //con = new System.Data.SqlClient.SqlConnection();
            //con.ConnectionString = "Data Source=" + DataSourceText + ";Initial Catalog=" + DataBaseText + ";User ID=sa;Password=";

            if (this.con.State == ConnectionState.Closed)
            {
                this.con.Open();
            }

            try
            {

                SqlCommand sqlCommInsertSPrestamo = new SqlCommand();
                sqlCommInsertSPrestamo = con.CreateCommand();
                sqlCommInsertSPrestamo.CommandText = @"INSERT INTO MA_SOLICITUD_PRESTA (cu_codtrab, cu_codprest, fu_fechas, fu_fecdesc, nu_monto, nu_cuota, nu_montxcuot, nu_tasa_int, cu_observacion, bu_status, bu_cancelado, bu_prestamocancel) VALUES(@CodEmp,@CodPrestamo,@Fecha,@FechaDes,@Monto,@Cuota,@MontoxCouta,@Tasa,@Observacion,@Estatus,@Cancelado,@Cancel)";
                sqlCommInsertSPrestamo.Parameters.Add("@CodEmp", SqlDbType.VarChar);
                sqlCommInsertSPrestamo.Parameters["@CodEmp"].Value = CodEmp;

                sqlCommInsertSPrestamo.Parameters.Add("@CodPrestamo", SqlDbType.VarChar);
                sqlCommInsertSPrestamo.Parameters["@CodPrestamo"].Value = Prestamo;

                sqlCommInsertSPrestamo.Parameters.Add("@Fecha", SqlDbType.VarChar);
                sqlCommInsertSPrestamo.Parameters["@Fecha"].Value = fecha_actual;

                sqlCommInsertSPrestamo.Parameters.Add("@FechaDes", SqlDbType.VarChar);
                sqlCommInsertSPrestamo.Parameters["@FechaDes"].Value = fecha_actual;

                sqlCommInsertSPrestamo.Parameters.Add("@Monto", SqlDbType.Float);
                sqlCommInsertSPrestamo.Parameters["@Monto"].Value = Monto;

                sqlCommInsertSPrestamo.Parameters.Add("@Cuota", SqlDbType.Int);
                sqlCommInsertSPrestamo.Parameters["@Cuota"].Value = 1;

                sqlCommInsertSPrestamo.Parameters.Add("@MontoxCouta", SqlDbType.Float);
                sqlCommInsertSPrestamo.Parameters["@MontoxCouta"].Value = Monto;

                sqlCommInsertSPrestamo.Parameters.Add("@Tasa", SqlDbType.Float);
                sqlCommInsertSPrestamo.Parameters["@Tasa"].Value = 0;

                sqlCommInsertSPrestamo.Parameters.Add("@Observacion", SqlDbType.VarChar);
                sqlCommInsertSPrestamo.Parameters["@Observacion"].Value = Observacion;

                sqlCommInsertSPrestamo.Parameters.Add("@Estatus", SqlDbType.VarChar);
                sqlCommInsertSPrestamo.Parameters["@Estatus"].Value = Estatus;

                sqlCommInsertSPrestamo.Parameters.Add("@Cancelado", SqlDbType.VarChar);
                sqlCommInsertSPrestamo.Parameters["@Cancelado"].Value = Cancelado;

                sqlCommInsertSPrestamo.Parameters.Add("@Cancel", SqlDbType.VarChar);
                sqlCommInsertSPrestamo.Parameters["@Cancel"].Value = 0;

                sqlCommInsertSPrestamo.ExecuteNonQuery();

                Bandera = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
            finally
            {

            }
            return Bandera;
        }

        public Boolean ProcesarPrestamo(string CodEmp, Double Monto, string fecha_actual, int IDSol)
        {
            bool Bandera = false;
            //con = new System.Data.SqlClient.SqlConnection();
            //con.ConnectionString = "Data Source=" + DataSourceText + ";Initial Catalog=" + DataBaseText + ";User ID=sa;Password=";
            try
            {
                if (this.con.State == ConnectionState.Closed)
                {
                    this.con.Open();
                }

                SqlCommand sqlCommProcesarPrestamo = new SqlCommand();
                sqlCommProcesarPrestamo = con.CreateCommand();
                sqlCommProcesarPrestamo.CommandText = @"INSERT INTO TR_PRESTAMO_PROCESADOS VALUES (@IdSol,@CodEmp,@CodPrestamo,@CodNom,@CodConcepto,@CodConceptoTipo,@Monto,@MontoCuota,@MontoSaldo,@FechaDescuento,@CodProceso,@MontoAbono)";

                sqlCommProcesarPrestamo.Parameters.Add("@IdSol", SqlDbType.VarChar);
                sqlCommProcesarPrestamo.Parameters["@IdSol"].Value = IDSol;

                sqlCommProcesarPrestamo.Parameters.Add("@CodEmp", SqlDbType.VarChar);
                sqlCommProcesarPrestamo.Parameters["@CodEmp"].Value = CodEmp;

                sqlCommProcesarPrestamo.Parameters.Add("@CodPrestamo", SqlDbType.VarChar);
                sqlCommProcesarPrestamo.Parameters["@CodPrestamo"].Value = Prestamo;

                sqlCommProcesarPrestamo.Parameters.Add("@CodNom", SqlDbType.VarChar);
                sqlCommProcesarPrestamo.Parameters["@CodNom"].Value = "0000000000";

                sqlCommProcesarPrestamo.Parameters.Add("@CodConcepto", SqlDbType.VarChar);
                sqlCommProcesarPrestamo.Parameters["@CodConcepto"].Value = "D"+Prestamo;

                sqlCommProcesarPrestamo.Parameters.Add("@CodConceptoTipo", SqlDbType.VarChar);
                sqlCommProcesarPrestamo.Parameters["@CodConceptoTipo"].Value = "0000000001";
                    
                sqlCommProcesarPrestamo.Parameters.Add("@Monto", SqlDbType.Float);
                sqlCommProcesarPrestamo.Parameters["@Monto"].Value = Monto;

                sqlCommProcesarPrestamo.Parameters.Add("@MontoCuota", SqlDbType.Float);
                sqlCommProcesarPrestamo.Parameters["@MontoCuota"].Value = 0;

                sqlCommProcesarPrestamo.Parameters.Add("@MontoSaldo", SqlDbType.Float);
                sqlCommProcesarPrestamo.Parameters["@MontoSaldo"].Value = Monto;

                sqlCommProcesarPrestamo.Parameters.Add("@FechaDescuento", SqlDbType.VarChar);
                sqlCommProcesarPrestamo.Parameters["@FechaDescuento"].Value = fecha_actual;

                sqlCommProcesarPrestamo.Parameters.Add("@CodProceso", SqlDbType.VarChar);
                sqlCommProcesarPrestamo.Parameters["@CodProceso"].Value = "00000000000";

                sqlCommProcesarPrestamo.Parameters.Add("@MontoAbono", SqlDbType.Float);
                sqlCommProcesarPrestamo.Parameters["@MontoAbono"].Value = 0;

                sqlCommProcesarPrestamo.ExecuteNonQuery();

                Bandera = true;
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
            finally
            {

            }
            return Bandera;
        }

        public bool ValidarUsuario()
        {
            ValUsuario valUsuario = new ValUsuario();
            valUsuario.ShowDialog();
            bool validacionUsuario = valUsuario.ValidacionUsuario;
            valUsuario.Close();
            return validacionUsuario;
        }

        public string Consecutivo(bool Siguiente)
        {

            string text = "";
            int num = 0;
            DataSet dataSet = new DataSet();
            string selectCommandText = "SELECT CU_Consecutivo FROM MA_CONSECUTIVOS WHERE CU_Campo='CorridaPrestamos'";

            if (this.con.State == ConnectionState.Closed)
            {
                this.con.Open();
            }

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommandText, this.con);
            sqlDataAdapter.Fill(dataSet, "Data");
            if (dataSet.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    DataRow dataRow = dataSet.Tables["Data"].Rows[i];
                    text = string.Concat(int.Parse(dataRow["CU_Consecutivo"].ToString()));
                    num = int.Parse(dataRow["CU_Consecutivo"].ToString());
                    int num2 = 9 - text.Length;
                    for (int j = 0; j <= num2 - 1; j++)
                    {
                        text = "0" + text;
                    }
                }
            }
            if (Siguiente)
            {
                try
                {

                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand = this.con.CreateCommand();
                    sqlCommand.CommandText = "UPDATE MA_CONSECUTIVOS SET CU_Consecutivo='" + (num + 1) + "' WHERE CU_Campo='CorridaPrestamos'";
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception arg)
                {
                    MessageBox.Show("Error: " + arg);
                }
                finally
                {
                }
            }
            
            return text;

        }

        public bool CrearCorridaPrestamo(string CodCorrida)
        {
            bool result = false;

            try
            {
                if (this.con.State == ConnectionState.Closed)
                {
                    this.con.Open();
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = this.con.CreateCommand();
                sqlCommand.CommandText = "INSERT INTO MA_CORRIDA_PRESTAMOS VALUES (@CodCorrida, @CodPrestamo, GETDATE())";
                sqlCommand.Parameters.Add("@CodCorrida", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodCorrida"].Value = CodCorrida;
                sqlCommand.Parameters.Add("@CodPrestamo", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodPrestamo"].Value = this.Prestamo;
                sqlCommand.ExecuteNonQuery();
                result = true;
            }
            catch (Exception arg)
            {
                MessageBox.Show("Error: " + arg);
            }
            finally
            {

            }
            return result;
        }
        public bool ValNominas()
        {
            bool flag = false;
            int num = 0;
            int num2 = 0;
            if (this.Nominas.CompareTo("") == 0)
            {
                flag = true;
            }
            if (!flag)
            {
                try
                {
                    if (this.con.State == ConnectionState.Closed)
                    {
                        this.con.Open();
                    }
                    string[] array = this.Nominas.Split(new char[]
					{
						','
					});
                    string[] array2 = array;
                    for (int i = 0; i < array2.Length; i++)
                    {
                        string text = array2[i];
                        string text2 = text.Trim();
                        if (text2.CompareTo("") != 0)
                        {
                            num2++;
                            DataSet dataSet = new DataSet();
                            string selectCommandText = "SELECT * FROM MA_NOMINA where cu_codigo = '" + text2 + "'";
                            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommandText, this.con);
                            sqlDataAdapter.Fill(dataSet, "Data");
                            if (dataSet.Tables[0].Rows.Count > 0)
                            {
                                num++;
                            }
                        }
                    }
                }
                catch (Exception arg)
                {
                    MessageBox.Show("Error: " + arg);
                }
                finally
                {

                }
                if (num == num2)
                {
                    flag = true;
                }
            }
            return flag;
        }

        public bool InsertSolPrestamo(string CodEmp, double Monto, string fecha_actual, string CodCorrida)
        {
            bool result = false;
            int num = 0;
            int num2 = 0;
            if (this.ProcesaPrestamo != 0)
            {
                num = 1;
                num2 = 1;
            }
            try
            {
                if (this.con.State == ConnectionState.Closed)
                {
                    this.con.Open();
                }

                SqlCommand sqlCommand = null;

                sqlCommand = this.con.CreateCommand();

                sqlCommand.CommandText = "INSERT INTO MA_SOLICITUD_PRESTA (cu_codtrab, cu_codprest, fu_fechas, fu_fecdesc, nu_monto, nu_cuota, nu_montxcuot, nu_tasa_int, cu_observacion, bu_status, bu_cancelado, bu_prestamocancel, cu_codcorrida) VALUES (@CodEmp, @CodPrestamo, @Fecha, @FechaDes, @Monto, @Cuota, @MontoxCouta, @Tasa, @Observacion, @Estatus, @Cancelado, @Cancel, @cu_codcorrida)";

                sqlCommand.Parameters.Add("@CodEmp", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodEmp"].Value = CodEmp;
                sqlCommand.Parameters.Add("@CodPrestamo", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodPrestamo"].Value = this.Prestamo;
                sqlCommand.Parameters.Add("@Fecha", SqlDbType.VarChar);
                sqlCommand.Parameters["@Fecha"].Value = fecha_actual;
                sqlCommand.Parameters.Add("@FechaDes", SqlDbType.VarChar);
                sqlCommand.Parameters["@FechaDes"].Value = fecha_actual;
                sqlCommand.Parameters.Add("@Monto", SqlDbType.Float);
                sqlCommand.Parameters["@Monto"].Value = Monto;
                sqlCommand.Parameters.Add("@Cuota", SqlDbType.Int);
                sqlCommand.Parameters["@Cuota"].Value = 1;
                sqlCommand.Parameters.Add("@MontoxCouta", SqlDbType.Float);
                sqlCommand.Parameters["@MontoxCouta"].Value = Monto;
                sqlCommand.Parameters.Add("@Tasa", SqlDbType.Float);
                sqlCommand.Parameters["@Tasa"].Value = 0;
                sqlCommand.Parameters.Add("@Observacion", SqlDbType.VarChar);
                sqlCommand.Parameters["@Observacion"].Value = this.Observacion;
                sqlCommand.Parameters.Add("@Estatus", SqlDbType.VarChar);
                sqlCommand.Parameters["@Estatus"].Value = num;
                sqlCommand.Parameters.Add("@Cancelado", SqlDbType.VarChar);
                sqlCommand.Parameters["@Cancelado"].Value = num2;
                sqlCommand.Parameters.Add("@Cancel", SqlDbType.VarChar);
                sqlCommand.Parameters["@Cancel"].Value = 0;
                sqlCommand.Parameters.Add("@cu_codcorrida", SqlDbType.VarChar);
                sqlCommand.Parameters["@cu_codcorrida"].Value = CodCorrida;

                sqlCommand.ExecuteNonQuery();

                result = true;
            }
            catch (Exception arg)
            {
                MessageBox.Show("Error: " + arg);
            }
            finally
            {
            }
            return result;
        }

        public string CodSolicitudPrestamo(string CodEmp)
        {
            string result = "";
            try
            {
                if (this.con.State == ConnectionState.Closed)
                {
                    this.con.Open();
                }
                DataSet dataSet = new DataSet();
                string selectCommandText = string.Concat(new string[]
				{
					"SELECT MAX(id) as Id FROM MA_SOLICITUD_PRESTA where cu_codtrab = '",
					CodEmp,
					"' and cu_codprest='",
					this.Prestamo,
					"'"
				});
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommandText, this.con);
                sqlDataAdapter.Fill(dataSet, "Data");
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        DataRow dataRow = dataSet.Tables["Data"].Rows[i];
                        result = dataRow["Id"].ToString();
                    }
                }
            }
            catch (Exception arg)
            {
                MessageBox.Show("Error: " + arg);
            }
            finally
            {

            }
            return result;
        }

        public bool ProcesarPrestamo(string CodEmp, double Monto, string fecha_actual, int IDSol, string CodCorrida)
        {
            bool result = false;

            try
            {
                if (this.con.State == ConnectionState.Closed)
                {
                    this.con.Open();
                }

                SqlCommand sqlCommand = null;
                sqlCommand = this.con.CreateCommand();

                sqlCommand.CommandText = "INSERT INTO TR_PRESTAMO_PROCESADOS VALUES (@IdSol, @CodEmp, @CodPrestamo, @CodNom, @CodConcepto, @CodConceptoTipo, @Monto, @MontoCuota, @MontoSaldo, @FechaDescuento, @CodProceso, @MontoAbono, @cu_codcorrida)";

                sqlCommand.Parameters.Add("@IdSol", SqlDbType.VarChar);
                sqlCommand.Parameters["@IdSol"].Value = IDSol;
                sqlCommand.Parameters.Add("@CodEmp", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodEmp"].Value = CodEmp;
                sqlCommand.Parameters.Add("@CodPrestamo", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodPrestamo"].Value = this.Prestamo;
                sqlCommand.Parameters.Add("@CodNom", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodNom"].Value = "0000000000";
                sqlCommand.Parameters.Add("@CodConcepto", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodConcepto"].Value = "D" + this.Prestamo;
                sqlCommand.Parameters.Add("@CodConceptoTipo", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodConceptoTipo"].Value = "0000000001";
                sqlCommand.Parameters.Add("@Monto", SqlDbType.Float);
                sqlCommand.Parameters["@Monto"].Value = Monto;
                sqlCommand.Parameters.Add("@MontoCuota", SqlDbType.Float);
                sqlCommand.Parameters["@MontoCuota"].Value = 0;
                sqlCommand.Parameters.Add("@MontoSaldo", SqlDbType.Float);
                sqlCommand.Parameters["@MontoSaldo"].Value = Monto;
                sqlCommand.Parameters.Add("@FechaDescuento", SqlDbType.VarChar);
                sqlCommand.Parameters["@FechaDescuento"].Value = fecha_actual;
                sqlCommand.Parameters.Add("@CodProceso", SqlDbType.VarChar);
                sqlCommand.Parameters["@CodProceso"].Value = "00000000000";
                sqlCommand.Parameters.Add("@cu_codcorrida", SqlDbType.VarChar);
                sqlCommand.Parameters["@cu_codcorrida"].Value = CodCorrida;
                sqlCommand.Parameters.Add("@MontoAbono", SqlDbType.Float);
                sqlCommand.Parameters["@MontoAbono"].Value = 0;

                sqlCommand.ExecuteNonQuery();

                result = true;

            }
            catch (Exception arg)
            {
                MessageBox.Show("Error: " + arg);
            }
            finally
            {

            }
            return result;
        }

        public bool ValidarSolicitudesActivas(string CodTrab)
        {
            bool result = false;
            DataSet dataSet = new DataSet();
            string selectCommandText = string.Concat(new string[]
			{
				"SELECT * From MA_SOLICITUD_PRESTA WHERE cu_codtrab = '",
				CodTrab,
				"' AND cu_codprest = '",
				this.Prestamo,
				"' AND bu_prestamocancel = 0 "
			});
            try
            {
                if (this.con.State == ConnectionState.Closed)
                {
                    this.con.Open();
                }
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommandText, this.con);
                sqlDataAdapter.Fill(dataSet, "Data");
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        DataRow dataRow = dataSet.Tables["Data"].Rows[i];
                        bool flag = bool.Parse(dataRow["bu_status"].ToString());
                        result = flag;
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception arg)
            {
                MessageBox.Show("Error: " + arg);
            }
            finally
            {

            }

            return result;
        }
        
        
    }
}
/*
    ESTRUCTURA DEL XML
<?xml version="1.0" encoding="UTF-8" ?>
<Configuracion>
	<Servidor>
		<DataSource>.</DataSource>
		<DataBase>NOMINASAMAN</DataBase>
	</Servidor>
	<Extras>
		<Porcentaje>45</Porcentaje>
		<CodPrestamo>001</CodPrestamo>
		<Nominas>0101,0102,0103</Nominas>
		<ProcesarPrestamo>1</ProcesarPrestamo>
		<Observacion>Aqui Incluya la Observacion del Prestamo</Observacion>
	</Extras>
</Configuracion>
*/