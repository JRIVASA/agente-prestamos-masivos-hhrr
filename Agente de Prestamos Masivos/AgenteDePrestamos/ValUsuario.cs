﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace AgenteDePrestamos
{
    public partial class ValUsuario : Form
    {
        public SqlConnection con;

        public string DataSourceText;

        public string DataBaseText;

        public double Porcentaje;

        public string Prestamo;

        public string Nominas;

        public string Observacion;

        public int ProcesaPrestamo;

        public List<string> DiasDePago = new List<string>();

        public bool ValidacionSolPrestamoActivo;

        public bool ValidacionUsuario;



        public ValUsuario()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.ValidacionUsuario = false;
            base.Hide();
        }

        private void ValUsuario_Load(object sender, EventArgs e)
        {
            string location = Assembly.GetExecutingAssembly().Location;
			string directoryName = Path.GetDirectoryName(location);
			if (File.Exists(directoryName + "/Setup.xml"))
			{
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.Load(directoryName + "/Setup.xml");
				XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("Configuracion");
				XmlNodeList elementsByTagName2 = ((XmlElement)elementsByTagName[0]).GetElementsByTagName("Servidor");
				XmlNodeList elementsByTagName3 = ((XmlElement)elementsByTagName[0]).GetElementsByTagName("Extras");
				foreach (XmlElement xmlElement in elementsByTagName2)
				{
					int num = 0;
					XmlNodeList elementsByTagName4 = xmlElement.GetElementsByTagName("DataSource");
					XmlNodeList elementsByTagName5 = xmlElement.GetElementsByTagName("DataBase");
					this.DataSourceText = elementsByTagName4[num].InnerText;
					this.DataBaseText = elementsByTagName5[num].InnerText;
					num++;
				}
				foreach (XmlElement xmlElement in elementsByTagName3)
				{
					int num = 0;
					XmlNodeList elementsByTagName6 = xmlElement.GetElementsByTagName("Porcentaje");
					this.Porcentaje = double.Parse(elementsByTagName6[num].InnerText) / 100.0;
					XmlNodeList elementsByTagName7 = xmlElement.GetElementsByTagName("CodPrestamo");
					this.Prestamo = elementsByTagName7[num].InnerText;
					XmlNodeList elementsByTagName8 = xmlElement.GetElementsByTagName("Nominas");
					this.Nominas = elementsByTagName8[num].InnerText;
					this.Nominas = this.Nominas.Trim();
					XmlNodeList elementsByTagName9 = xmlElement.GetElementsByTagName("ProcesarPrestamo");
					this.ProcesaPrestamo = int.Parse(elementsByTagName9[num].InnerText);
					XmlNodeList elementsByTagName10 = xmlElement.GetElementsByTagName("Observacion");
					this.Observacion = elementsByTagName10[num].InnerText;
					num++;
				}
				string text = DateTime.Today.ToString("yyyyMMdd");
				this.con = new SqlConnection();
        }

    }

        private void button1_Click(object sender, EventArgs e)
        {
            string text = this.txt_Usuario.Text;
            string text2 = this.txt_Password.Text;
            this.con.ConnectionString = string.Concat(new string[]
			{
				"Data Source=",
				this.DataSourceText,
				";Initial Catalog=",
				this.DataBaseText,
				";User ID=sa;Password="
			});
            try
            {
                DataSet dataSet = new DataSet();
                string selectCommandText = "SELECT login_name as Usuario, password as Clave FROM MA_USUARIOS WHERE login_name = '" + text + "'";
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommandText, this.con);
                sqlDataAdapter.Fill(dataSet, "Data");
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        DataRow dataRow = dataSet.Tables["Data"].Rows[i];
                        string value = dataRow["Clave"].ToString();
                        if (text2.Equals(value, StringComparison.Ordinal))
                        {
                            this.ValidacionUsuario = true;
                            base.Hide();
                        }
                        else
                        {
                            MessageBox.Show("Usuario o contraseña invalida");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Usuario o contraseña invalida");
                }
            }
            catch (Exception arg)
            {
                MessageBox.Show("Error: " + arg);
            }
        }
    }
}
